package com.alozano.siteme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class SelectedPlace extends AppCompatActivity {

    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_place);

        ImageView placeImg = findViewById(R.id.placeImg);
        TextView placeName = findViewById(R.id.placeName);
        TextView placeAddr = findViewById(R.id.placeAddr);

        bundle = getIntent().getExtras();

        placeName.setText(bundle.getString("placeName"));
        placeAddr.setText(bundle.getString("placeAddr"));
    }

    public void onUbiClicked(View v) {
        Intent intent = new Intent(SelectedPlace.this, MapsActivity.class);
        intent.putExtra("placeName", bundle.getString("placeName"));
        intent.putExtra("placeLat", bundle.getDouble("placeLat"));
        intent.putExtra("placeLng", bundle.getDouble("placeLng"));
        startActivity(intent);
    }
}
