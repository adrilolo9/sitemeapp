package com.alozano.siteme;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.alozano.siteme.adapter.Grid_BaseAdapter;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private LocationRequest mLocationRequest;

    private long UPDATE_INTERVAL = 10000;
    private long FASTEST_INTERVAL = 2000;
    private int PERMISSION_ID = 44;

    private GridView gridView;
    Grid_BaseAdapter grid_baseAdapter;

    private MyLocation loc = null;

    int [] images = {R.drawable.dish_pasta,R.drawable.dish_pizza,R.drawable.dish_salad,R.drawable.dish_soup,R.drawable.dish_dessert,R.drawable.dish_curry,R.drawable.dish_hamburguer,R.drawable.dish_kebab,
            R.drawable.dish_rice};

    String [] data = {"Pasta","Pizza","Amanida","Sopa","Pastissos",
            "Curry","Hamburguesa","Kebab","Arròs"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (checkPermissions()){
            startLocationUpdates();
        }else{
            requestPermissions();
        }

        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.gridView);

        grid_baseAdapter = new Grid_BaseAdapter(this,images,data);

        gridView.setAdapter(grid_baseAdapter);

        setupListViewListener();

    }

    // Attaches a long click listener to the listview
    private void setupListViewListener() {
        gridView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapter, View item, int pos, long id) {
                        Double lat = loc.getPlaceLat();
                        Double lng = loc.getPlaceLng();

                        getPlaces(data[pos], lat.toString(),lng.toString());
                    }
                }
        );
    }

    private void getPlaces(final String searchItem, final String lat, final String lng){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    URL apiURL = new URL("https://api.foursquare.com/v2/venues/explore?client_id=TZRB22AW2BXM1UWOXZK4MMV1ORKJPBTOCZ5X4BNPEZFCUJYC&client_secret=MHCMBNNPJ4141V1OSKZJZ03EVVLTUGSPU3AA2RUZ1V2YKWBW&v=20180323&ll=" + lat + "," + lng + "&query=" + searchItem + "&limit=1");
                    HttpsURLConnection urlConnection = (HttpsURLConnection) apiURL.openConnection();

                    int responseCode = urlConnection.getResponseCode();

                    if (urlConnection.getResponseCode() == 200) {
                        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                        urlConnection.getInputStream()));

                        StringBuilder response = new StringBuilder();
                        String currentLine;

                        while ((currentLine = in.readLine()) != null)
                            response.append(currentLine);

                        in.close();

                        JSONObject jsonResponse = new JSONObject(response.toString());
                        JSONObject place = jsonResponse.getJSONObject("response").getJSONArray("groups").getJSONObject(0).getJSONArray("items").getJSONObject(0).getJSONObject("venue");
                        String placeName = place.getString("name");
                        JSONArray placeAddressArray = place.getJSONObject("location").getJSONArray("formattedAddress");
                        String placeAddress = "";
                        Double placeLat = place.getJSONObject("location").getDouble("lat");
                        Double placeLng = place.getJSONObject("location").getDouble("lng");
                        JSONObject photosObj = place.getJSONObject("photos");

                        if (photosObj.getInt("count") > 0){
                            //get pictures, still dont know which format they are in cuz i didnt find any request that had any.
                        }

                        for (int i = 0; i < placeAddressArray.length(); i++){
                            placeAddress += placeAddressArray.getString(i) + "\n";
                        }

                        Intent intent = new Intent(MainActivity.this, SelectedPlace.class);
                        intent.putExtra("placeName", placeName);
                        intent.putExtra("placeAddr", placeAddress);
                        intent.putExtra("placeLat", placeLat);
                        intent.putExtra("placeLng", placeLng);
                        startActivity(intent);

                    } else {
                        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                        urlConnection.getErrorStream()));

                        StringBuilder response = new StringBuilder();
                        String currentLine;

                        while ((currentLine = in.readLine()) != null)
                            response.append(currentLine);

                        in.close();

                        Log.w("CANNOT GET PLACE INFO", response.toString());

                        Toast.makeText(getApplicationContext(),"Cannot get place info, please try again.",Toast.LENGTH_LONG).show();
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean checkPermissions(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }

    private void requestPermissions(){
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                startLocationUpdates();
            }
        }
    }

    // Trigger new location updates at interval
    protected void startLocationUpdates() {
        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }

    public void onLocationChanged(Location location) {
        loc = new MyLocation(location.getLatitude(), location.getLongitude());
    }
}
