package com.alozano.siteme;

public class MyLocation {
    private Double placeLat;
    private Double placeLng;
    public MyLocation(Double placeLat, Double placeLng){
        this.placeLat = placeLat;
        this.placeLng = placeLng;
    }

    public Double getPlaceLat(){
        return this.placeLat;
    }
    public Double getPlaceLng(){
        return this.placeLng;
    }
}
